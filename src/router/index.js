import Vue from 'vue';
import Router from 'vue-router';
import List from '@/components/List';

Vue.use(Router);

/* eslint-disable no-new */
export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'List',
      component: List,
      children: [
        {
          path: ':id',
          name: 'Item',
          component: List,
        },
      ],
    },
  ],
});
